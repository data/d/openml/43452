# OpenML dataset: student-grade-pass-or-fail-prediction

https://www.openml.org/d/43452

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is based on dipam7/student-grade-prediction
Dataset
General information
This data approach student achievement in secondary education of two Portuguese schools. The data attributes include student grades, demographic, social and school-related features) and it was collected by using school reports and questionnaires. Two datasets are provided regarding the performance in two distinct subjects: Mathematics (mat) and Portuguese language (por). In [Cortez and Silva, 2008], the two datasets were modeled under binary/five-level classification and regression tasks. 
Important note the target attribute G3 has a strong correlation with attributes G2 and G1. This occurs because G3 is the final year grade (issued at the 3rd period), while G1 and G2 correspond to the 1st and 2nd period grades. It is more difficult to predict G3 without G2 and G1, but such prediction is much more useful (see paper source for more details).
Attribute information
Important note unlike the original dataset, this one converted all binary values to numerical bool values. Moreover, it doesn't have attributes Mjob, Fjob, reason, guardian
school - student's school (binary: 'GP' - Gabriel Pereira or 'MS' - Mousinho da Silveira) (bool: 1 if it's GP else 0)
sex - student's sex (binary: 'F' - female or 'M' - male) (bool: 1 if it's F else 0)
age - student's age (numeric: from 15 to 22)
address - student's home address type (binary: 'U' - urban or 'R' - rural) (bool: 1 if it's U else 0)
famsize - family size (binary: 'LE3' - less or equal to 3 or 'GT3' - greater than 3) (bool: 1 if it's LE3 else 0)
Pstatus - parent's cohabitation status (binary: 'T' - living together or 'A' - apart) (bool: 1 if it's T else 0)
Medu - mother's education (numeric: 0 - none, 1 - primary education (4th grade), 2  5th to 9th grade, 3  secondary education or 4  higher education)
Fedu - father's education (numeric: 0 - none, 1 - primary education (4th grade), 2  5th to 9th grade, 3  secondary education or 4  higher education)
traveltime - home to school travel time (numeric: 1 - 1 hour)
studytime - weekly study time (numeric: 1 - 10 hours)
failures - number of past class failures (numeric: n if 1=n3, else 4)
schoolsup - extra educational support (binary: yes or no) (bool: 1 if it's Yes else 0)
famsup - family educational support (binary: yes or no) (bool: 1 if it's Yes else 0)
paid - extra paid classes within the course subject (Math or Portuguese) (binary: yes or no) (bool: 1 if it's Yes else 0)
activities - extra-curricular activities (binary: yes or no) (bool: 1 if it's Yes else 0)
nursery - attended nursery school (binary: yes or no) (bool: 1 if it's Yes else 0)
higher - wants to take higher education (binary: yes or no) (bool: 1 if it's Yes else 0)
internet - Internet access at home (binary: yes or no) (bool: 1 if it's Yes else 0)
romantic - with a romantic relationship (binary: yes or no) (bool: 1 if it's Yes else 0)
famrel - quality of family relationships (numeric: from 1 - very bad to 5 - excellent)
freetime - free time after school (numeric: from 1 - very low to 5 - very high)
goout- going out with friends (numeric: from 1 - very low to 5 - very high)
Dalc - workday alcohol consumption (numeric: from 1 - very low to 5 - very high)
Walc - weekend alcohol consumption (numeric: from 1 - very low to 5 - very high)
health - current health status (numeric: from 1 - very bad to 5 - very good)
absences - number of school absences (numeric: from 0 to 93)
G1 - first period grade (numeric: from 0 to 20)
G2 - second period grade (numeric: from 0 to 20)
G3 - final grade (numeric: from 0 to 20)
pass - (bool: 1 if that student passes else 0) output target
Generator
I assume you have downloaded the original dataset.
import pandas as pd
pd.set_option('display.max_columns', 100)

 Read from original dataset
df = pd.read_csv('student-mat.csv')

 Drop columns that contain nominal values
df = df.drop(columns=['Mjob', 'Fjob', 'reason', 'guardian'])

 Convert binary values to numerical values
df['school'] = (df['school'] == 'GP').astype('int')
df['sex'] = (df['sex'] == 'F').astype('int')
df['address'] = (df['address'] == 'U').astype('int')
df['famsize'] = (df['famsize'] == 'LE3').astype('int')
df['Pstatus'] = (df['Pstatus'] == 'T').astype('int')
df['schoolsup'] = (df['schoolsup'] == 'yes').astype('int')
df['famsup'] = (df['famsup'] == 'yes').astype('int')
df['paid'] = (df['paid'] == 'yes').astype('int')
df['activities'] = (df['activities'] == 'yes').astype('int')
df['nursery'] = (df['nursery'] == 'yes').astype('int')
df['higher'] = (df['higher'] == 'yes').astype('int')
df['internet'] = (df['internet'] == 'yes').astype('int')
df['romantic'] = (df['romantic'] == 'yes').astype('int')

 Add a column to indicate who fail
df['pass'] = (df['G3'] = 10).astype('int')

 Save to student-mat-pass-or-fail.csv
with open('student-mat-pass-or-fail.csv', 'w') as f:
    f.write(df.to_csv(index=False))

Task
With all attributes except G3, please predict pass. 
Citation
P. Cortez and A. Silva. Using Data Mining to Predict Secondary School Student Performance. In A. Brito and J. Teixeira Eds., Proceedings of 5th FUture BUsiness TEChnology Conference (FUBUTEC 2008) pp. 5-12, Porto, Portugal, April, 2008, EUROSIS, ISBN 978-9077381-39-7.
Web Link

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43452) of an [OpenML dataset](https://www.openml.org/d/43452). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43452/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43452/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43452/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

